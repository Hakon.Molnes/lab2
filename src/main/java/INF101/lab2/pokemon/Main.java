package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1 = new Pokemon("Totodile", 36, 7);
    public static Pokemon pokemon2 = new Pokemon("Grimer", 25  , 6);
    public static void main(String[] args) {
        ///// Oppgave 6
        // Have two pokemon fight until one is defeated

        while (pokemon1.isAlive() && pokemon2.isAlive()){
            if (pokemon1.getCurrentHP() == 0 || pokemon2.getCurrentHP() == 0){
                System.out.println("Battle is over!");
                break;
            }
            pokemon1.attack(pokemon2);
            pokemon2.attack(pokemon1);
        }
        

        
    }
}
